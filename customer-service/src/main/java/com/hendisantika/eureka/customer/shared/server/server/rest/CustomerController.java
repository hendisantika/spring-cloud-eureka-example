package com.hendisantika.eureka.customer.shared.server.server.rest;

import com.hendisantika.eureka.customer.shared.Customer;
import com.hendisantika.eureka.customer.shared.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/17
 * Time: 8:35 AM
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class CustomerController implements CustomerService {

    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    private final List<Customer> customers;

    public CustomerController() {
        customers = new LinkedList<>();
        customers.add(new Customer(1, "Hendi", "Santika"));
        customers.add(new Customer(2, "Uzumaki", "Naruto"));
        customers.add(new Customer(3, "Uchiha", "Sasuke"));
        customers.add(new Customer(4, "Hatake", "Kakashi"));
        customers.add(new Customer(5, "Haruno", "Sakura"));
        customers.add(new Customer(6, "Namikaze", "Minato"));
        customers.add(new Customer(7, "Hiruzen", "Sarutobi"));
        customers.add(new Customer(8, "Hyuuga", "Neji"));
        customers.add(new Customer(9, "Hyuuga", "Hinata"));
        customers.add(new Customer(10, "Uchiha", "Madara"));
    }

    public Customer getCustomer(@PathVariable int id) {

        logger.debug("reading customer with id " + id);

        Optional<Customer> customer = customers.stream().filter(customer1 -> customer1.getId() == id).findFirst();

        return customer.get();

    }

}
