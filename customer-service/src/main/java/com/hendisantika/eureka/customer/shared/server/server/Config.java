package com.hendisantika.eureka.customer.shared.server.server;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/17
 * Time: 8:10 AM
 * To change this template use File | Settings | File Templates.
 */

@EnableDiscoveryClient
@EnableWebMvc
@Configuration
public class Config {

}
