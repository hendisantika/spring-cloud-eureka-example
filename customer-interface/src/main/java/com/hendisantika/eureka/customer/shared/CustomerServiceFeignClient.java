package com.hendisantika.eureka.customer.shared;


import org.springframework.cloud.openfeign.FeignClient;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/3/17
 * Time: 7:58 AM
 * To change this template use File | Settings | File Templates.
 */

@FeignClient(value = "customer-service", fallback = CustomerServiceFeignClientFallback.class)
public interface CustomerServiceFeignClient extends CustomerService {

}
